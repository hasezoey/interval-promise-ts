#!/bin/sh
#
# You should only run this, when newly Cloned or these Branches/Remotes dont already Exists!
#

git remote add barets ssh://git@gitlab.sylvi:5002/hasezoey/bare_ts.git
git remote set-url --push barets nope # because we dont want to push to here
git remote add upstream https://github.com/andyfleming/interval-promise.git
git remote set-url --push upstream nope # because we dont want to push to here

# Fetch Everything
git fetch

# Pull from every Repo into a diffrent branch
git pull barets master:barets
git pull upstream master:upstream

# Set the branches to Track these Repos
git checkout barets
git branch --set-upstream-to=barets/master 

git checkout upstream
git branch --set-upstream-to=upstream/master
