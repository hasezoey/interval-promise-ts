# Interval Promise re-write to TS

its a rewrite to TS
Original by [Andy Fleming](https://github.com/andyfleming/interval-promise)

using my(hasezoey) TS config & lint options

# The Following is from the original Readme

## Overview

This library provides a simple mechanism for running a promise with a given amount of time between executions.

#### Standard Javascript » setInterval()

![traditional interval](https://user-images.githubusercontent.com/721038/33246371-9d0a6e56-d2c8-11e7-9787-cd67354c9f38.png)

#### interval-promise » interval()

![interval promise](https://user-images.githubusercontent.com/721038/33246370-9cf05a20-d2c8-11e7-816c-744ee6b5a094.png)

## Usage

**Simple example using async-await**

```js
const interval = require('interval-promise')

// Run a function 10 times with 1 second between each iteration
const id = interval.interval(async () => {
    await someOtherPromiseReturningFunction()
    await another()
}, 1000, {iterations: 10})

// clear it
await clearInterval(id);
```

```ts
import { interval, clearInterval } from "interval-promise";

// Run a function 10 times with 1 second between each iteration
const id: string = interval(async () => {
    await someOtherPromiseReturningFunction()
    await another()
}, 1000, {iterations: 10})

// clear it
await clearInterval(id);
```

## API

```ts
interval(cb: function, intervalLength: number | Infinity, options: IOptions): Promise<void>;
```

### Arguments

<table>
    <thead>
        <tr>
            <th>Argument</th>
            <th>Attritubes</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code><b>cb</b></code></td>
            <td><i>function</i></td>
            <td><b>Required</b><br>Function to execute for each interval. <i>MUST</i> return a promise.<br><br>Two arguments are passed to this function.
                <ul>
                    <li><code>iterationNumber</code> <i>number</i> — The iteration number (starting at 0)</li>
                    <li><code>stop</code> <i>function</i> — used to "stop" (skipping all remaining iterations)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><code><b>intervalLength</b></code></td>
            <td><i>number | function</i></td>
            <td><b>Required</b><br>Length in ms to wait between iterations. Should be (or return) a non-negative integer.<br><br>If a function is used, one parameter <code>iterationNumber</code> (starting at 1) is passed.</td>
        </tr>
        <tr>
            <td><code><b>options</b></code></td>
            <td><i>object</i></td>
            <td>Optional settings (detailed below).
        <tr>
            <td><code>options.<b>iterations</b></code></td>
            <td><i>number</i></td>
            <td><b>Default: </b><code>Infinity</code><br>The number of times to execute the function. Must be Infinity or an integer greater than 0.<br/>(note: counting from 0)</td>
        </tr>
        <tr>
            <td><code>options.<b>stopOnError</b></code></td>
            <td><i>boolean</i></td>
            <td><b>Default: </b><code>true</code><br>If true, no subsequent calls will be made. The promise returned by interval() will be rejected and pass through the error thrown.</td> 
        </tr>
    </tbody>
</table>

```ts
clearInterval(id: string): Promise<void>;
```

### Arguments

<table>
    <thead>
        <tr>
            <th>Argument</th>
            <th>Attritubes</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code><b>id</b></code></td>
            <td><i>string</i></td>
            <td><b>Required</b><br>ID returned by an interval
            </td>
        </tr>
    </tbody>
</table>

### More on "intervalLength" as a function

It will be called with "0" once (the initial call for the first run), and after that "Infinity" if it is set to run Infinite, otherwise a normal number  

## Project Values

* **Approachability** — Basic usage should be concise and readable.
* **Debuggability** — Error feedback should be helpful and error handling options should be flexible.
* **Stability** — Functionality should be well-tested and reliable.
