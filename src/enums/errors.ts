export enum IntervalErrors {
    CB_IS_NOT_A_FUNCTION = "Provided Callback is not a Function!",
    INTERVAL_IS_NOT_RIGHT_TYPE = "Provided IntervalLenght is not a number nor a function!",
    INTERVAL_LENGHT_IS_NOT_HIGH_ENOUGH = "Provied IntervalLength is not over 10ms! Reasons:\na function was provided that didnt return a number\nor the provided number was not over 10ms!",
    OPTIONS_ITERATIONS_TYPEERROR = "Provided Options.Iterations is not of type number!",
    OPTIONS_ITERATIONS_RANGEERROR = "Provided Options.Iterations is must be higher than 1!",
    OPTIONS_STOPONERROR_TYPEERROR = "Provided Options.StopOnError is not of type boolean!"
}

export enum IntervalInPromise {
    NOT_FOUND_IN_ARRAY = "find: not found in array! (this should never happen)",
    RETURN_IS_NOT_A_PROMISE = "Provided CB didnt return a Promise!"
}

export enum ClearIntervalErrors {
    ID_IS_NOT_A_STRING = "Provided ID is not a string!"
}
