import * as Debug from "debug";
import { EventEmitter } from "events";
import "source-map-support/register";
import * as uniqid from "uniqid";
import { ClearIntervalErrors, IntervalErrors, IntervalInPromise } from "./enums/errors";
const debug: Debug.Debugger = Debug("interval:main");

type IIntervalCallback = (stop: () => void, iterationNumber: number) => Promise<any>;

const events: EventEmitter = new EventEmitter();
events.on("cI", (id) => {
    debug("event cI called with [%s]", id);
});

interface IIntervalArray {
    id: string;
    stop: boolean;
    interval: Promise<void>;
}

interface IIntervalOptions {
    /** Default: Infinity */
    iterations?: number;
    /** Default: true */
    stopOnError?: boolean;
}

interface IIntervalOptionsInternal extends IIntervalOptions {
    /** Default: Infinity */
    iterations: number;
    /** Default: true */
    stopOnError: boolean;
}

type IIntervalLength = number | ((iteration: number) => number);

/**
 * This is the Array with the intervals
 * This should NOT be used outside of tests!
 */
export const intervalArray: IIntervalArray[] = [];

/**
 * Execute a Function at an Interval, when the previous finished
 * Returns the ID of the interval (for clearInterval)
 */
export function interval(cb: IIntervalCallback, intervalLength: IIntervalLength): string;
export function interval(cb: IIntervalCallback, intervalLength: IIntervalLength, options: IIntervalOptions): string;
export function interval(cb: IIntervalCallback, intervalLength: IIntervalLength, options?: IIntervalOptions): string {
    debug("interval: called");
    // Section Checking
    if (typeof cb !== "function") throw new TypeError(IntervalErrors.CB_IS_NOT_A_FUNCTION);
    if (typeof intervalLength !== "function" && typeof intervalLength !== "number") throw new TypeError(IntervalErrors.INTERVAL_IS_NOT_RIGHT_TYPE);
    let il: number = validateNumberFunction(intervalLength, 0);
    if (options && typeof options.iterations !== "undefined") {
        if (typeof options.iterations !== "number") throw new TypeError(IntervalErrors.OPTIONS_ITERATIONS_TYPEERROR);
        if (options.iterations < 1) throw new RangeError(IntervalErrors.OPTIONS_ITERATIONS_RANGEERROR);
    }
    if ((options && typeof options.stopOnError !== "undefined") && typeof options.stopOnError !== "boolean") {
        throw new TypeError(IntervalErrors.OPTIONS_STOPONERROR_TYPEERROR);
    }
    // End Sections Checking
    debug("interval: Checking Finished");

    const opt: IIntervalOptionsInternal = Object.assign({}, { // assign default options
        iterations: Infinity,
        stopOnError: true
    }, options);

    const id: string = uniqid();

    intervalArray.push({
        id,
        stop: false,
        interval: new Promise((res) => res()) // dummy promise
    });

    intervalArray[intervalArray.findIndex((x) => x.id == id)].interval = new Promise(async (rootRes, rootRej) => {
        debug("interval: creating Root Promise");
        let found: IIntervalArray = find(id);
        let currentIteration: number = 0;

        async function next(): Promise<boolean> {
            debug("interval:.next called [%d] [%s]", currentIteration, id);
            const index: number = intervalArray.findIndex((x) => x.id == found.id); // to shorten it
            if (index < 0 || !intervalArray[index]) {
                debug("interval:.next failed to get the index, index is [%s]", index);
                rootRes();
                return true;
            }
            const tmp: boolean = typeof intervalArray[index].stop === "boolean" && intervalArray[index].stop;
            if ((currentIteration >= opt.iterations && currentIteration != Infinity) || tmp) {
                debug("interval:.next: quiting because of stop or target Reached [%s]", currentIteration, id);
                rootRes();
                await clearInterval(id);

                return true;
            // disabling this rule below, because it is awaiting, and not returning immediatly
            // tslint:disable-next-line:unnecessary-else
            } else {
                found = find(id);
                if (opt.iterations === Infinity) currentIteration = Infinity; // this is because the first run should be run immediatly (otherwise currentIteration would already be Infinity)
                else currentIteration += 1;
                il = validateNumberFunction(intervalLength, currentIteration);

                return false;
            }
        }

        async function call(): Promise<boolean> {
            debug("interval:.call called [%s]", id);
            if (currentIteration !== 0) await timeout(il, id); // await the time
            const index: number = intervalArray.findIndex((x) => x.id == found.id);
            if (index < 0 || intervalArray[index].stop) {
                rootRes();
                return true;
            }

            const val: Promise<any> | any = cb(() => {
                intervalArray[intervalArray.findIndex((x) => x.id == id)].stop = true;
            }, currentIteration);

            if (!(val instanceof Promise)) {
                rootRej(new TypeError(IntervalInPromise.RETURN_IS_NOT_A_PROMISE));
                return true;
            }

            try {
                debug("interval:.call awaiting val [%s]", id);
                await val;
                debug("interval:.call awaiting val [%s] finished", id);

                return false;
            } catch (err) {
                debug("interval:.call errored [%s]", id);
                if (opt.stopOnError) rootRej(err);

                return true;
            }
        }

        async function startEndless(): Promise<void> {
            debug("interval:.startEndless called [%s]", id);
            if (await call()) return; // call "call" this is just the same as below, and just for the "stopOnError" option
            debug("interval:.startEndless: call finished (with false) [%s]", id);
            if (await next()) return; // call "next" and if true(stop) then stop this function
            debug("interval:.startEndless: next finished (with false) [%s]", id);
            await startEndless(); // start it again
        }

        await timeout(1, "nope"); // to return the id before the first execution
        startEndless().catch((err) => {
            // i dont know if this will ever execute, but i will let it here for completeness
            debug("interval:Promise.resolve().catch error (please report this, it should never fire) [%s]", id);
            throw err;
        });
    });

    return id;
}

/**
 * Internal Promise-Timer
 */
async function timeout(duration: number, id: string): Promise<void> {
    return new Promise((res) => {
        const tm: NodeJS.Timeout = setTimeout(() => {
            events.removeListener("cI", bound);
            res();
        }, duration);
        const bound: (eid: string) => void = (eid: string) => {
            if (eid === id) {
                events.removeListener("cI", bound);
                clearTimeout(tm);
                res();
            }
        };
        events.on("cI", bound);
    });
}

/**
 * Internal Function to dynamicly validate & return the intervallength
 */
function validateNumberFunction(intervalLength: IIntervalLength, iteration: number): number {
    let il: number = 0;
    if (typeof intervalLength === "number") il = Math.trunc(intervalLength);
    else if (typeof intervalLength === "function") il = intervalLength(iteration);
    if (il < 10) throw new RangeError(IntervalErrors.INTERVAL_LENGHT_IS_NOT_HIGH_ENOUGH);

    return il;
}

/**
 * Internal Function to find in the Array
 */
function find(id: string): IIntervalArray {
    const found: IIntervalArray | undefined = intervalArray.find((x) => x.id == id);
    if (!found) {
        debug("interval: %s", IntervalInPromise.NOT_FOUND_IN_ARRAY);
        throw new Error(IntervalInPromise.NOT_FOUND_IN_ARRAY);
    }

    return found;
}

/**
 * Clears the Interval
 */
export async function clearInterval(id: string): Promise<void> {
    debug("interval.clearInterval called");
    if (typeof id !== "string") throw new TypeError(ClearIntervalErrors.ID_IS_NOT_A_STRING);

    const found: IIntervalArray | undefined = intervalArray.find((x) => x.id == id);
    if (!found) {
        debug("interval.clearInterval: found was emtpy, returning");
        return;
    }

    intervalArray[intervalArray.findIndex((x) => x.id == found.id)].stop = true;

    debug("interval.clearInterval: awaiting finish of the function [%s]", id);
    events.emit("cI", id);
    await found.interval;
    intervalArray.splice(intervalArray.findIndex((x) => x.id == found.id), 1);
    debug("interval.clearInterval: finished awaiting");

    return;
}
