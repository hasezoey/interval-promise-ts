import { expect, use } from "chai";
import * as cap from "chai-as-promised";
import { ClearIntervalErrors, IntervalErrors, IntervalInPromise } from "../src/enums/errors";
import { clearInterval, interval, intervalArray } from "../src/interval";
// tslint:disable:no-empty

use(cap);

describe("Error Testing", () => {
    it("should throw TypeError if provied first argument is not a function", () => {
        // @ts-ignore
        expect(() => interval(0)).to.throw(IntervalErrors.CB_IS_NOT_A_FUNCTION);
    });

    it("should throw TypeError if intervalLenght is not a number or a function", () => {
        // @ts-ignore
        expect(() => interval(async () => {})).to.throw(IntervalErrors.INTERVAL_IS_NOT_RIGHT_TYPE);
    });

    it("should throw RangeError if provided number is below 10", () => {
        expect(() => interval(async () => {}, -1, {})).to.throw(RangeError, IntervalErrors.INTERVAL_LENGHT_IS_NOT_HIGH_ENOUGH);
    });

    it("should throw TypeError if options.iterations is not a number", () => {
        // @ts-ignore
        expect(() => interval(async () => {}, 1000, {iterations: ""})).to.throw(IntervalErrors.OPTIONS_ITERATIONS_TYPEERROR);
    });

    it("should throw RangeError if options.iterations is below 1", () => {
        expect(() => interval(async () => {}, 1000, {iterations: 0})).to.throw(IntervalErrors.OPTIONS_ITERATIONS_RANGEERROR);
    });

    it("should throw TypeError if options.stopOnError is not a boolean", () => {
        // @ts-ignore
        expect(() => interval(async () => {}, 1000, {stopOnError: ""})).to.throw(IntervalErrors.OPTIONS_STOPONERROR_TYPEERROR);
    });

    it("should throw TypeError if cb dosnt return a Promise", (done) => {
        // @ts-ignore
        const id: string = interval(() => {}, 1000);
        intervalArray[intervalArray.findIndex((x) => x.id == id)].interval.catch((err) => {
            expect(err).to.be.an.instanceOf(TypeError, IntervalInPromise.RETURN_IS_NOT_A_PROMISE);
            done();
        });
    });

    it("should throw TypeError if clearInterval(id) id is not a string", (done) => {
        // @ts-ignore
        clearInterval().catch((err) => {
            expect(err).to.be.an.instanceOf(TypeError, ClearIntervalErrors.ID_IS_NOT_A_STRING);
            done();
        });
    });
});
