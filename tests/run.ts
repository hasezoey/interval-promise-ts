import { expect, use } from "chai";
import * as cap from "chai-as-promised";
import { clearInterval, clearInterval as cI, interval, intervalArray } from "../src/interval";
// tslint:disable:no-invalid-this

use(cap);

async function timeout(duration: number): Promise<void> {
    return new Promise((res) => setTimeout(res, duration));
}

describe("Run Testing", () => {
    it("should run 3 times, without Errors", async function(): Promise<void> {
        this.timeout(4000);
        let counter: number = 0;
        const id: string = interval(async () => {
            counter++;
        }, 1000, { iterations: 2 });

        await timeout(3000);
        expect(id).to.be.a("string");
        expect(counter).to.be.equal(3);
        return;
    });

    it("should run Infinite times (testing clearInterval), without Errors", async function(): Promise<void> {
        this.timeout(5000);
        let counter: number = 0;
        const id: string = interval(async () => {
            counter++;
        }, 1000);

        await timeout(3000);
        expect(id).to.be.a("string");
        await cI(id);
        expect(counter).to.be.above(2); // on slower PC's this could fail
        return;
    });

    it("should run 2 times (testing IntervalLength as function), without Errors", async function(): Promise<void> {
        this.timeout(4000);
        let counter: number = 0;
        const id: string = interval(async () => {
            counter++;
        }, () => 1000, { iterations: 1 });

        await timeout(2000);
        expect(id).to.be.a("string");
        expect(counter).to.be.equal(2);
        return;
    });

    it("should just return (clearInterval without creating an interval), without Errors", async function(): Promise<void> {
        this.timeout(4000);
        expect(clearInterval("")).to.eventually.not.be.rejectedWith();
        return;
    });

    it("should run stop, without Errors", async () => {
        let counter: number = 0;
        const id: string = interval(async (stop) => {
            counter++;
            stop();
        }, 1000);

        expect(id).to.be.a("string");
        expect(intervalArray[intervalArray.findIndex((x) => x.id == id)].interval).to.eventually.not.rejectedWith();
        await intervalArray[intervalArray.findIndex((x) => x.id == id)].interval;
        expect(counter).to.be.equal(1);
        return;
    });

    it("should run with this", (done) => {
        class Test {
            public hello: string = "hello world";

            public async test(stop: () => void): Promise<void> {
                expect(this).to.have.property("hello");
                expect(this.hello).to.be.equal("hello world");
                stop();
            }
        }

        const instance: Test = new Test();
        const id: string = interval(instance.test.bind(instance), 1000, { iterations: 1 });

        expect(id).to.be.a("string");
        expect(intervalArray[intervalArray.findIndex((x) => x.id == id)].interval).to.eventually.not.rejectedWith();
        expect(intervalArray[intervalArray.findIndex((x) => x.id == id)].interval).to.eventually.be.fulfilled.and.notify(done);
    });

    it("should run without Error if intervalArray dosnt include it anymore", async (): Promise<void> => {
        // tslint:disable-next-line:no-empty
        const id: string = interval(async () => {}, 1000);
        expect(intervalArray[intervalArray.findIndex((x) => x.id == id)].interval).to.eventually.not.rejectedWith();
        const int: Promise<void> = intervalArray[intervalArray.findIndex((x) => x.id == id)].interval;
        // @ts-ignore
        intervalArray.splice(intervalArray.findIndex((x) => x.id == id), 1);
        return int;
    });
});
